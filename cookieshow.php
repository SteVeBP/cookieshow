<?php

/**
 * @copyright Copyright (C) 2013 Independents IS. All rights reserved.
 * @license GNU General Public License version 2 or later
 */
defined('JPATH_BASE') or die;
jimport('joomla.plugin.plugin');

/**
 * This is a cookie show plugin. It adds information to comfort EU law regarding
 * informing users of cookie usage on a webpage.
 */
class plgSystemCookieshow extends JPlugin {

	/**
	 * Method to handle the "onAfterRender" event and add information to the specified
	 * part of the webpage.
	 *
	 * 
	 * @return bool True if the adding process was succesful
	 *
	 * @since 1.0
	 */
	function onAfterRender() {
		if (!JFactory::getApplication()->isSite()) {
			return true;
		}
		$user = & JFactory::getUser();
		if ((!isset($_COOKIE['cookieshow']) /* && !$user->getParam("cookie_info_show") */ ) || ($this->params->get('debug') == 1)) {
			$document = JFactory::getDocument();
			$this->loadLanguage();
			$user_text = "<p>" . JText::_('PLG_SYSTEM_COOKIESHOW_INFO_TEXT') . "</p>";
			$close_text = '<p>' . JText::_('PLG_SYSTEM_COOKIESHOW_CLOSE_TEXT') . '</p>';
			$to_append = '<div id="cookie_info">' . $user_text . $close_text . '</div>';

			$cookie_time = $this->params->get('cookie_time');

			setcookie("cookieshow", 1, $cookie_time);
			JRequest::setVar("cookieshow", 1, 'COOKIE');
			JResponse::appendBody($to_append);
		}
	}

	/**
	 * 	This method is called to add script to the head part of the webpage to animate
	 * 	the added element.
	 */
	function onBeforeCompileHead() {
		if (!JFactory::getApplication()->isSite()) {
			return true;
		}
		$user = & JFactory::getUser();
		if ((!isset($_COOKIE['cookieshow']) /* && !$user->getParam("cookie_info_show") */ ) || ($this->params->get('debug') == 1)) {
			$user->setparam("cookie_info_show", true);
			$user->save(true);
			$image_bg = $this->params->get('bg-image', 'none');
			$position = $this->params->get('position');
			$position_x = $this->params->get('position-x');
			$position_y = $this->params->get('position-y');
			$width = $this->params->get('width');
			$text_align = $this->params->get('text-align');

			//$this->params->get('border-color');
			//$this->params->get('border-size');
			$border_radius = $this->params->get('border-radius');
			$padding = $this->params->get('padding');


			$font_size = $this->params->get('font-size');
			$line_height = $this->params->get('line-height');
			$font_color = $this->params->get('font-color');
			$effect = $this->params->get('effect');
			$effect_time = $this->params->get('effect-time');

			$script =
					  '
      $.noConflict();
      jQuery(document).ready(function() {
	jQuery("#cookie_info").click(function() {
	    jQuery(this).' . $effect . '(' . $effect_time . ');
	});
      });';
			$style =
					  '#cookie_info {
	position: ' . $position . ';
	padding: ' . $padding . 'px;
	';
			if ($position_y == "middle") {
				$style .= 'top:50%;';
			} elseif ($position_y == "bottom") {
				$style .= 'bottom:0;';
			} else {
				$style .= 'top:0;';
			};
			if ($position_x == "left") {
				$style .= 'left:0;';
			} elseif ($position_y == "right") {
				$style.='right:0;';
			} else {
				$style.= 'left:' . ((100 - $width) / 2) . '%;';
			}
			// ABOVE CODE CALCULATES CENTER POSITION

			$borders = array(
				 "top-left"		 => true,
				 "top-right"		 => true,
				 "bottom-left"	 => true,
				 "bottom-right"	 => true);
			/*
			 *  determining which borders should NOT be rounded, because they touch
			 *  the edge of the screen 
			 */

			if ($position_x == "left") {
				$borders["top-left"] = false;
				$borders["bottom-left"] = false;
			} elseif ($position_x == "right") {
				$borders["top-right"] = false;
				$borders["bottom-right"] = false;
			}
			if ($position_y == "top") {
				$borders["top-left"] = false;
				$borders["top-right"] = false;
			} elseif ($position_y == "bottom") {
				$borders["bottom-left"] = false;
				$borders["bottom-right"] = false;
			}
			foreach ($borders as $key => $value) {
				if ($value == true) {
					$style.= 'border-' . $key . '-radius: ' . $border_radius . 'px;';
				}
			}
			$style.= '	
	width: ' . $width . '%;
	background-image: url("' . $image_bg . '");
	z-index: 999;
	text-align: ' . $text_align . ';';
			$style.='
      }
      #cookie_info > p {
	line-height: ' . $line_height . 'px;
	font-size: ' . $font_size . 'px;
	color: #' . $font_color . ';
      }';

			$document = JFactory::getDocument();

			$document->addscript("http://code.jquery.com/jquery-1.9.1.min.js");
			$document->addscriptdeclaration($script);
			$document->addstyledeclaration($style);
		}
	}

}

